import React, {Component} from 'react';
import {connect} from 'react-redux';
import './TodoList.css';
import TodoItem from "../../components/TodoItem/TodoItem";
import Loader from "../../components/Loader/Loader";

import {getTodoItems, newTodoHandler, postTodoItem, removeTodoItem} from "../../store/actions";

class TodoList extends Component {
  componentDidMount() {
    this.props.getTodoItems();
  }

  render() {
    let todoList = this.props.todoList.map(item => {
      return <TodoItem key={item.id} item={item} clicked={this.props.removeTodoItem} />
    });

    if (this.props.loading) todoList = <Loader />;

    return (
      <div className="TodoList">
        <input type="text" className="TodoInput" value={this.props.newTodo.value} onChange={(e) => this.props.newTodoHandler(e)}/>
        <button className="Btn" onClick={this.props.postTodoItem}>Add</button>
        {todoList}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    todoList: state.todoList,
    newTodo: state.newTodo,
    loading: state.loading
  }
};

const mapDispatchToProps = dispatch => {
  return {
    newTodoHandler: (e) => dispatch(newTodoHandler(e)),
    getTodoItems: () => dispatch(getTodoItems()),
    postTodoItem: (value) => dispatch(postTodoItem(value)),
    removeTodoItem: (id) => dispatch(removeTodoItem(id)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);